# Static Analysis Study Clubhouse

Welcome to the Static Analysis study clubhouse! This is a project to organize the [Static Analysis
Group's](https://about.gitlab.com/handbook/engineering/development/secure/static-analysis/) study of any topic
that makes them better at doing Static Analysis. All are welcome to join in the conversation and to study with
us whether or not they are a part of the [Static Analysis Group](https://about.gitlab.com/handbook/engineering/development/secure/static-analysis/).

## What to study

We will be using
[Issues](https://gitlab.com/gitlab-org/secure/static-analysis-be/study-club/clubhouse/-/issues) in this
project to help post ideas as to what we should study in the future as well as decide what to study next.

## Where to discuss

The https://gitlab.com/gitlab-org/secure/static-analysis-be/study-club group will host projects per thing
we're studying where we can have discussion asynchronously. As we study things, we should update the list
below with a link to the project.

### Study Projects

1. [TBD](https://gitlab.com/gitlab-org/secure/static-analysis-be/study-club/clubhouse/-/issues/1)
